# massive_multiplayer_online_server application

How to start the massive_multiplayer_online_server application
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/myfirstdropwizardapp-1.0-SNAPSHOT.jar server test-config.yml`
1. To check that your application is running enter url `http://localhost:8080/player/get_players`

Health Check
---

To see your applications health enter url `http://localhost:8081/healthcheck`
