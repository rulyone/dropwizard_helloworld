package com.n3twork;

import com.n3twork.core.Game;
import com.n3twork.core.network.GameServer;
import com.n3twork.db.PingDAO;
import com.n3twork.db.PlayerDAO;
import com.n3twork.health.DatabaseHealthCheck;
import com.n3twork.resources.PlayerResource;
import io.dropwizard.Application;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import java.util.concurrent.Executors;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MMOGApplication extends Application<MMOGWorldConfiguration> {
    
    private Logger log = LoggerFactory.getLogger(MMOGApplication.class);

    public static void main(final String[] args) throws Exception {
        new MMOGApplication().run(args);
    }

    @Override
    public String getName() {
        return "massive_multiplayer_online_server";
    }

    @Override
    public void initialize(final Bootstrap<MMOGWorldConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final MMOGWorldConfiguration configuration,
                    final Environment environment) {
        //jdbi3 registration
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, configuration.getDataSourceFactory(), "h2");
        
        PingDAO pingDAO = jdbi.onDemand(PingDAO.class);
        
        PlayerDAO playerDAO = jdbi.onDemand(PlayerDAO.class);
        playerDAO.createPlayerTable();
        
        //resources       
        final PlayerResource playerResource = new PlayerResource(playerDAO);
        environment.jersey().register(playerResource);
        
        //healthchecks
        final DatabaseHealthCheck databaseHealthCheck = new DatabaseHealthCheck(pingDAO);
        environment.healthChecks().register("database", databaseHealthCheck);
        
        //starting udpserver, don'w know if this is a good practice using dropwizard... ToDO.
        int port = 5555;        
        Game game = new Game(playerDAO);
        GameServer gameServer = new GameServer(port, game);
        Executors.newSingleThreadExecutor().submit(gameServer);
        Executors.newSingleThreadExecutor().submit(game);
    }

}
