/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.client;

import com.n3twork.core.network.PacketTypes;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rulyone
 */
public class TestUdpClient {
    
    private static Logger log = LoggerFactory.getLogger(TestUdpClient.class);
    
    public static void main(String[] args) {
        DatagramSocket socket = null;
        try {
            socket = new DatagramSocket();
        } catch (SocketException e) {
            log.error("error opening datagramsocket", e);
        }
        InetAddress address = null;
        try {
            address = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            log.error("error getting localhost address", e);
        }
        
        int packetTypeSize = Short.BYTES;
        int packetSizeSize = Integer.BYTES;
        int timestampSize = Long.BYTES;
        int playerIdSize = Long.BYTES;
        String joinMsg = "Keep calm, and keep playing.";
        byte[] joinMsgBuf = joinMsg.getBytes(Charset.forName("utf-8"));
        int joinMsgByteSize = joinMsgBuf.length;
        byte[] joinCommand = new byte[packetTypeSize + packetSizeSize + timestampSize + playerIdSize + joinMsgByteSize];
        ByteBuf byteBuf = Unpooled.wrappedBuffer(joinCommand);
        byteBuf.clear();
        byteBuf.writeShort(PacketTypes.JOIN_PACKET.getPacketType());
        byteBuf.writeInt(timestampSize + playerIdSize + joinMsgByteSize);
        byteBuf.writeLong(System.currentTimeMillis());
        byteBuf.writeLong(2L);
        byteBuf.writeBytes(joinMsgBuf);

        DatagramPacket packet = new DatagramPacket(byteBuf.array(), joinCommand.length, address, 5555);
        
        try {
            log.info("sending packet..." + packet);
            socket.send(packet);
        } catch (IOException e) {
            log.error("error sending packet", e);
        }


    }
    
}
