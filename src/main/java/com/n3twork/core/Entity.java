/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.core;

/**
 * Spatial Partition based on http://gameprogrammingpatterns.com/spatial-partition.html#when-to-use-it
 * 
 * @author rulyone
 */
public abstract class Entity {
    protected Long id;
    protected float x;
    protected float y;
    
    protected Grid grid;
    protected Entity previous;
    protected Entity next;
    
    protected int maxHealth;
    protected int currentHealth;
    
    protected int attackStrength;
    
    protected Entity() {
    }
    
    public Entity(Long id, Grid grid, float x, float y, int maxHealth, int currentHealth, int attackStrength) { 
        this.id = id;
        this.grid = grid;
        this.x = x;
        this.y = y;
        this.previous = null;
        this.next = null;
        this.maxHealth = maxHealth;
        this.currentHealth = currentHealth;
        this.attackStrength = attackStrength;
    }
        
    public void teleport(float x, float y) {
        this.grid.teleport(this, x, y);
    }
    
    //GETTERS & SETTERS
    public int getAttackStrength() {
        return attackStrength;
    }

    public void setAttackStrength(int attackStrength) {
        this.attackStrength = attackStrength;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public int getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(int currentHealth) {
        this.currentHealth = currentHealth;
    }

    public Long getId() {
        return id;
    }

    public Grid getGrid() {
        return grid;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }

    public Entity getPrevious() {
        return previous;
    }

    public void setPrevious(Entity previous) {
        this.previous = previous;
    }

    public Entity getNext() {
        return next;
    }

    public void setNext(Entity next) {
        this.next = next;
    }
}
