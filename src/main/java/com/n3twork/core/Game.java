/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.core;

import com.n3twork.core.commands.JoinCommand;
import com.n3twork.core.commands.TeleportCommand;
import com.n3twork.db.PlayerDAO;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rulyone
 */
public class Game implements Callable<Object> {
    
    private Logger log = LoggerFactory.getLogger(Game.class);
    
    //the grid of the game in 2D.
    private Grid grid;
    //id - player (key - value)
    private Map<Long, Player> players;
    
    //Queue for receiving commands from clients.
    private ArrayBlockingQueue<GameCommand> commandsQueue;
    //Array list with commands currently in queue for processing.
    List<GameCommand> currentCommands;
    private boolean running = false;
    
    private PlayerDAO playerDAO;
        
    public Game(PlayerDAO playerDAO) {
        grid = new Grid();
        //considering that this gamecommands will be processed @ 60FPS, we'll hardly have more than 100 commands in this queue. make it configurable later.
        commandsQueue = new ArrayBlockingQueue<>(100);
        currentCommands = new ArrayList<>(100);
        this.players = new HashMap<>();
        //ToDo: config FPS
        this.playerDAO = playerDAO;
    }
    
    @Override
    public Object call() throws Exception {
        this.startGameLoop();
        return null;
    }
    
    public void stopGame() {
        //ToDo: do some cleanup
        this.running = false;
    }
    
    private void startGameLoop() {
        this.running = true;
        double ns = 1000000000.0 / 60.0;
        double delta = 0;
        
        long lastTime = System.nanoTime();
//        long timer = System.currentTimeMillis();
        
        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            
            while (delta >= 1) {
                updateGameState();
                broadcastGameState();
                delta--;
            }
        }
    }
    
    private void updateGameState() {
        //first we process all gamecommands. ToDo: only process commands near the current game tick.         
        commandsQueue.drainTo(currentCommands);
        for (GameCommand command : currentCommands) {
            try {
                interpretCommand(command);
            } catch (InvalidCommandException ex) {
                log.error("Error executing gamecommand.", ex);
            }
        }
        //now we handle melee attacks.... ToDo: add attack delay, for now it will be 1 attack per frame
        grid.handleMelee();
        currentCommands.clear();
    }
    
    private void broadcastGameState() {
        //ToDo: broadcast current state to all players... or better yet, delta state.
        
    }

    public void queueCommandForExecution(GameCommand command) {
        log.debug("queueCommandForExecution playerId:" + command.getPlayerId());
        this.commandsQueue.add(command);
    }
    
    private void interpretCommand(GameCommand command) throws InvalidCommandException {
        if (command instanceof TeleportCommand) {
            TeleportCommand teleportCommand = (TeleportCommand)command;
            teleportPlayer(teleportCommand);
        }else if(command instanceof JoinCommand) {
            JoinCommand joinCommand = (JoinCommand) command;
            joinPlayer(joinCommand);            
        }else{
            log.debug("unknown command...");
        }
    }
    
    private void teleportPlayer(TeleportCommand teleportCommand) {
        //ToDo: implement tick based server game, using player speed to move to the desired point.
        log.debug("teleportPlayer: " + teleportCommand.getPlayerId());
        try {
            Player player = players.get(teleportCommand.getPlayerId());
            Point teleportTo = teleportCommand.getPayload();
            player.teleport(teleportTo.x, teleportTo.y);
        } catch (Exception ex) {
            log.error("You were trying to teleport to an invalid location... cheaters never win in the long run.");
        }
    }
    
    private void joinPlayer(JoinCommand joinCommand) {
        long playerId = joinCommand.getPlayerId();
        log.debug("joinCommand: " + playerId);
        try {
            Player player = playerDAO.findPlayerById(playerId); //only place to call database... the other is when player disconnects. ToDo
            grid.add(player);
            players.put(playerId, player);
        } catch (Exception ex) {
            log.error("Couldn't add player to the map.", ex);
        }
    }
}
