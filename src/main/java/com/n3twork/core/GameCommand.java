/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.core;

/**
 *
 * @author rulyone
 * @param <P> type of payload used by this command.
 */
public interface GameCommand<P> {
    
    public P getPayload();
    public void setPayload(P payload);
    public long getPlayerId();
    public void setPlayerId(long playerId);
    public long getTimestamp();
    public void setTimestamp(long timestamp);
    
}
