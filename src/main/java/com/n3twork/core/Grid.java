/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Spatial partition based on http://gameprogrammingpatterns.com/spatial-partition.html#when-to-use-it
 * 
 * @author rulyone
 */
public class Grid {
    
    private Logger log = LoggerFactory.getLogger(Grid.class);
    
    private static final int NUM_CELLS = 8;
    private static final int CELL_SIZE = 8;
    private static final int ATTACK_DISTANCE = 1;
    
    private Entity[][] cells = new Entity[NUM_CELLS][NUM_CELLS];
    
    public Grid() {
    }
    
    public void teleport(Entity entity, float x, float y) {
        // See which cell it was in.
        int oldCellX = (int) (entity.x / CELL_SIZE);
        int oldCellY = (int) (entity.y / CELL_SIZE);
        
        // See which cell it's moving to.
        int cellX = (int) (x / CELL_SIZE);
        int cellY = (int) (y / CELL_SIZE);
        
        entity.x = x;
        entity.y = y;
        
        // If it didn't change cells, we're done.
        if (oldCellX == cellX && oldCellY == cellY)
            return;

        // Unlink it from the list of its old cell.
        if (entity.previous != null) {
            entity.previous.next = entity.next;
        }

        if (entity.next != null) {
            entity.next.previous = entity.previous;
        }

        // If it's the head of a list, remove it.
        if (cells[oldCellX][oldCellY] == entity) {
            cells[oldCellX][oldCellY] = entity.next;
        }

        // Add it back to the grid at its new cell.
        add(entity);
        
    }

    public void add(Entity entity) {
        // Determine which grid cell it's in.
        int cellX = (int) (entity.x / CELL_SIZE);
        int cellY = (int) (entity.y / CELL_SIZE);
        // Add to the front of list for the cell it's in.
        entity.previous = null;
        entity.next = cells[cellX][cellY];
        cells[cellX][cellY] = entity;
        
        if (entity.next != null) {
            entity.next.previous = entity;
        }
        entity.setGrid(this);
    }
    
    public void remove(Entity entity) {
        //Determine which grid cell it's in
        int cellX = (int) (entity.x / CELL_SIZE);
        int cellY = (int) (entity.y / CELL_SIZE);
        // Unlink it from the list of its old cell
        if (entity.previous != null) {
            entity.previous.next = entity.next;
        }
        if (entity.next != null) {
            entity.next.previous = entity.previous;
        }
        //If it's the head of a list, remove it.
        if (cells[cellX][cellY] == entity) {
            cells[cellX][cellY] = entity.next;
        }
        entity.setGrid(null);
    }
    
    public void handleMelee() {
        for (int x = 0; x < NUM_CELLS; x++) {
            for (int y = 0; y < NUM_CELLS; y++) {
                handleCell(x, y);
            }
        }
    }
    
    private void handleCell(int x, int y) {        
        Entity entity = cells[x][y];
        while (entity != null) {
            // Handle other units in this cell.
            handleUnit(entity, entity.next);
            
            // Also try the neighboring cells.
            if (x > 0 && y > 0) {
                handleUnit(entity, cells[x - 1][y - 1]);
            }
            if (x > 0) {
                handleUnit(entity, cells[x - 1][y]);
            }
            if (y > 0) {
                handleUnit(entity, cells[x][y - 1]);
            }  
            if (x > 0 && y < NUM_CELLS - 1) {
                handleUnit(entity, cells[x - 1][y + 1]);
            }

            entity = entity.next;
        }
    }
    
    private void handleUnit(Entity entity, Entity other) {
        while (other != null) {
            if (distance(entity, other) < ATTACK_DISTANCE) {
                handleAttack(entity, other);
            }
            other = other.next;
        }
    
    }
    
    private int distance(Entity entity, Entity other) {
        return (int) Math.hypot(entity.x - other.x, entity.y - other.y);
    }
    
    private void handleAttack(Entity entity, Entity other) {
        //for now we print current health  and attack strenght of each entity
        log.debug("Entity " +entity.id + " hp-attack: " + entity.currentHealth + "-" + entity.attackStrength + ", Entity " + other.id + " hp-attack: " + other.currentHealth + "-" + other.attackStrength);
        entity.currentHealth -= other.attackStrength;
        other.currentHealth -= entity.attackStrength;
        log.debug("Entity " + entity.id + " received " + other.attackStrength + " damage from Entity " + other.id);
        log.debug("Entity " + other.id + " received " + entity.attackStrength + " damage from Entity " + entity.id);
        if (entity.currentHealth <= 0) {
            //DIED
            log.debug("Entity " + entity.id + " just died.");
            //remove from grid
            remove(entity);
        }
        if (other.currentHealth <= 0) {
            //DIED
            log.debug("Entity " + other.id + " just died.");
            //remove from grid
            remove(other);
        }
    }
    
    //GETTERS & SETTERS
    public Entity[][] getCells() {
        return cells;
    }

    public void setCells(Entity[][] cells) {
        this.cells = cells;
    }
    
}
