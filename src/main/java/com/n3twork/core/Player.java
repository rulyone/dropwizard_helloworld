/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.core;

/**
 *
 * @author rulyone
 */
public class Player extends Entity {
    
    private String nickname;

    public Player(String nickname, Long id, Grid grid, float x, float y, int maxHealth, int currentHealth, int attackStrength) {
        super(id, grid, x, y, maxHealth, currentHealth, attackStrength);
    }
    
    public Player(Long id, String nickname, float x, float y, int maxHealth, int currentHealth, int attackStrength) {
        super(id, null, x, y, maxHealth, currentHealth, attackStrength);
        this.nickname = nickname;
    }
    
    public Player() {
        super();
    }

    public String getNickname() {
        return nickname;
    }
    
}
