/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.core.commands;

import com.n3twork.core.GameCommand;


/**
 * ToDo: add to lib so that we can use it in clients, and add some magic number to make sure we process only game packets.
 * @author rulyone
 */
public class JoinCommand implements GameCommand<String> {
    
    private long playerId;
    private String joinMessage;
    private long timestamp;

    public JoinCommand(long timestamp, long playerId, String joinMessage) {
        this.timestamp = timestamp;
        this.playerId = playerId;
        this.joinMessage = joinMessage;
    }
    
    @Override
    public String getPayload() {
        return joinMessage;
    }

    @Override
    public void setPayload(String payload) {
        this.joinMessage = payload;
    }

    @Override
    public long getPlayerId() {
        return playerId;
    }

    @Override
    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public String getJoinMessage() {
        return joinMessage;
    }

    public void setJoinMessage(String joinMessage) {
        this.joinMessage = joinMessage;
    }


    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
    
}
