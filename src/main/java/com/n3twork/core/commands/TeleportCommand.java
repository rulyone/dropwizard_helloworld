/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.core.commands;

import com.n3twork.core.GameCommand;
import java.awt.Point;

/**
 *
 * @author rulyone
 */
public class TeleportCommand implements GameCommand<Point>{
    
    private long playerId;
    private Point teleportTo;
    private long timestamp;
    
    public TeleportCommand(long timestamp, long playerId, Point moveTo) {
        this.teleportTo = moveTo;
        this.playerId = playerId;
        this.timestamp = timestamp;
    }

    @Override
    public Point getPayload() {
        return teleportTo;
    }

    @Override
    public void setPayload(Point payload) {
        this.teleportTo = payload;
    }

    @Override
    public long getPlayerId() {
        return playerId;
    }

    @Override
    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    @Override
    public long getTimestamp() {
        return this.timestamp;
    }

    @Override
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
    
    public Point getTeleportTo() {
        return teleportTo;
    }

    public void setTeleportTo(Point moveTo) {
        this.teleportTo = moveTo;
    }
    
}
