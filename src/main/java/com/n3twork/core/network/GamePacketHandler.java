/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.core.network;

import com.n3twork.core.Game;
import com.n3twork.core.GameCommand;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rulyone
 */
public class GamePacketHandler extends  SimpleChannelInboundHandler<GameCommand>{
    
    Logger log = LoggerFactory.getLogger(GamePacketHandler.class);
    
    private Game game;
    
    public GamePacketHandler(Game game) {
        this.game = game;
    }
    
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GameCommand gameCommand) throws Exception {
        log.info("channelRead0");
        game.queueCommandForExecution(gameCommand);
    }
    
}
