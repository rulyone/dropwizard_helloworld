/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.core.network;

import com.n3twork.core.Game;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import java.net.InetAddress;
import java.util.concurrent.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rulyone
 */
public class GameServer implements Callable<Object> {

    private Logger log = LoggerFactory.getLogger(GameServer.class);
    
    private int port;
    
    private Game game;
    
    public GameServer(int port, Game game) {
        this.port = port;
        this.game = game;
    }
    
    @Override
    public Object call() throws Exception {
        final NioEventLoopGroup group = new NioEventLoopGroup();
        try {
            final Bootstrap b = new Bootstrap();
            //UDP
            b.group(group).channel(NioDatagramChannel.class)
                    .option(ChannelOption.SO_BROADCAST, true)
                    .handler(new ChannelInitializer<NioDatagramChannel>() {
                        @Override
                        public void initChannel(final NioDatagramChannel ch) throws Exception {

                            ChannelPipeline p = ch.pipeline();
                            p.addLast(new GamePacketDecoder());
                            p.addLast(new GamePacketHandler(game));

                        }
                    });
            // Bind and start to accept incoming connections.
            InetAddress address  = InetAddress.getLocalHost();
            log.info("Waiting for message @ port: {} on: {}", port , address.getHostAddress());
            b.bind(address,port).sync().channel().closeFuture().await();
        } finally {
            log.debug("Close resources?");
        }
        return null;
    }
    
}
