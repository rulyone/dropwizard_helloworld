/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.core.network;

/**
 *
 * @author rulyone
 */
public class InvalidPacketException extends RuntimeException {

    public InvalidPacketException() {
        super();
    }
    
    public InvalidPacketException(String msg) {
        super(msg);
    }
}
