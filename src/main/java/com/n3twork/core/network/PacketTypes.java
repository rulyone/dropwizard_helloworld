/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.core.network;

/**
 *
 * @author rulyone
 */
public enum PacketTypes {
    MOVE_PACKET((short)1),
    JOIN_PACKET((short)2);
    
    private short packetType;
    
    PacketTypes(short packetType) {
        this.packetType = packetType;
    }
    
    public short getPacketType() {
        return this.packetType;
    }
}
