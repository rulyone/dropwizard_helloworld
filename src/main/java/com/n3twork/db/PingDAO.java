/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.db;

import org.jdbi.v3.sqlobject.statement.SqlQuery;

/**
 *
 * @author rulyone
 */
public interface PingDAO {
    
    @SqlQuery("SELECT 1")
    int ping();
    
}
