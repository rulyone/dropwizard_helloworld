/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.db;

import com.n3twork.core.Player;
import java.util.List;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

/**
 *
 * @author rulyone
 */
public interface PlayerDAO {
    
    @SqlUpdate("create table if not exists player (id BIGINT AUTO_INCREMENT PRIMARY KEY, nickname VARCHAR_IGNORECASE, x float4, y float4, max_health INT, current_health INT, attack_strength INT)")
    void createPlayerTable();
    
    @SqlUpdate("insert into player (nickname, x, y, max_health, current_health, attack_strength) VALUES (:nickname, :x, :y, :max_health, :current_health, :attack_strength)")
    void createPlayer(@Bind("nickname") String nickname, @Bind("x") float x, @Bind("y") float y, @Bind("max_health") int maxHealth, @Bind("current_health") int currentHealth, @Bind("attack_strength") int attackStrength);
    
    @SqlQuery("select id, nickname, x, y, max_health, current_health, attack_strength FROM player")
    @RegisterRowMapper(PlayerMapper.class)
    List<Player> findAllPlayers();
    
    @SqlQuery("select id, nickname, x, y, max_health, current_health, attack_strength FROM player WHERE id = :id")
    @RegisterRowMapper(PlayerMapper.class)
    Player findPlayerById(@Bind("id") long id);
    
}
