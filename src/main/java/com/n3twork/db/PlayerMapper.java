/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.db;

import com.n3twork.core.Player;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

/**
 *
 * @author rulyone
 */
public class PlayerMapper implements RowMapper<Player> {

    @Override
    public Player map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new Player(rs.getLong("id"), rs.getString("nickname"), rs.getFloat("x"), rs.getFloat("y"), rs.getInt("max_health"), rs.getInt("current_health"), rs.getInt("attack_strength"));
    }
    
}
