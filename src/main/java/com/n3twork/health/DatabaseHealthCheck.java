/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.health;

import com.codahale.metrics.health.HealthCheck;
import com.n3twork.db.PingDAO;

/**
 *
 * @author rulyone
 */
public class DatabaseHealthCheck extends HealthCheck {
    
    final PingDAO pingDAO;
    
    public DatabaseHealthCheck(PingDAO pingDAO) {
        this.pingDAO = pingDAO;
    }
    
    @Override
    protected Result check() throws Exception {
        //not the best solution.
        if (pingDAO.ping() == 1) {
            return Result.healthy();
        }
        //It should have thrown an exception before this, ¿how does healtcheck handle exceptions? ToDo
        return Result.unhealthy("Wrong ping response...");
    }
    
}
