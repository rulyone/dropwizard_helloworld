/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.n3twork.resources;

import com.codahale.metrics.annotation.Timed;
import com.n3twork.core.Player;
import com.n3twork.db.PlayerDAO;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rulyone
 */
@Path("/player")
@Produces(MediaType.APPLICATION_JSON)
public class PlayerResource {
    
    private Logger log = LoggerFactory.getLogger(PlayerResource.class);
    
    private PlayerDAO playerDAO;
    
    public PlayerResource(PlayerDAO playerDAO) {
        this.playerDAO = playerDAO;
    }
    
    @GET
    @Timed
    @Path("/get_players")
    public Response getPlayers() {
        List<Player> players = playerDAO.findAllPlayers();
        return Response.ok(players).build();
    }
    
    @GET
    @Timed
    @Path("/get_player_by_id/{playerId}")
    public Response getPlayerById(@PathParam("playerId") Long playerId) {
        Player player = playerDAO.findPlayerById(playerId);
        return Response.ok(player).build();
    }
    
    @POST
    @Timed
    @Path("/create_player")
    public Response createPlayer(
            @FormParam("nickname") String nickname, 
            @FormParam("max_health") int maxHealth,
            @FormParam("current_health") int currentHealth,
            @FormParam("attack_strength") int attackStrength
    ) {
        playerDAO.createPlayer(nickname, 0, 0, maxHealth, currentHealth, attackStrength);
        return Response.ok("Player created").build();
    }
    
}
